<?php
/////////////////////////////////////////////////////////
// Copyright © 2013 Vernon de Goede & Ilija Ivankovic  //
/////////////////////////////////////////////////////////

// Voorkom dat er foutmeldingen gegeven worden omdat de aanvraag niet van hetzelfde domein komen.
header('Access-Control-Allow-Origin: *'); 
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

$testMap = $_GET['test'];

if($testMap == "hoofdsteden") {
$arr = array(
	array('Amsterdam', 'Nederland', 'Amsterdam, Netherlands', 'stad', 9), 
	array('Rotterdam', 'Nederland', 'Rotterdam, Netherlands', 'stad', 7), 
	array('Utrecht', 'Nederland', 'Utrecht, Netherlands', 'stad', 7), 
	array('Groningen', 'Nederland', 'Groningen, Netherlands', 'stad', 7), 
	array('Eindhoven', 'Nederland', 'Eindhoven, Netherlands', 'stad', 7),
	array('Tilburg', 'Nederland', 'Tilburg, Netherlands', 'stad', 7),
	array('Almere', 'Nederland', 'Almere, Netherlands', 'stad', 7),
	array('Breda', 'Nederland', 'Breda, Netherlands', 'stad', 7),
	array('Nijmegen', 'Nederland', 'Nijmegen, Netherlands', 'stad', 7),
	array('Enschede', 'Nederland', 'Enschede, Netherlands', 'stad', 7)
	);
}

shuffle($arr);
echo json_encode($arr);
?>